#!/bin/bash
# call: ./createNextStepsOverview.sh -f <modified-file> -o <owner-of-the-file>

USER=bjoern
NEXTCLOUDDATA=/mnt/nextcloud-data
OVERVIEW_RELATIVE=/$USER/files/Notes/nextcloud/Projects/Overview.md
OVERVIEW=$NEXTCLOUDDATA$OVERVIEW_RELATIVE
PROJECTS=$NEXTCLOUDDATA/$USER/files/Notes/nextcloud/Projects
LOCKFILE=/tmp/createNextStepsOverview.lock
FILE=''

while [ -f $LOCKFILE ]
do
    sleep 5
done

echo "lock" > $LOCKFILE

# Collect Next Steps from the Projects and add it to the overview file
collectNextSteps () {
    PROJECT=$(basename "$1")
    if [ -f "$1"/"Next Steps.md" ]; then
	items=`tail -n +3 "$1"/"Next Steps.md"`
	test=`echo "${items//[$'\t\r\n ']}"`
	case $test in
	    (*[a-zA-Z]*)
		echo $'\n\n'"## $PROJECT" >> $OVERVIEW
		echo "$items" >> $OVERVIEW
		echo $'\n\n'"[Edit]($PROJECT/Next Steps.md)" >> $OVERVIEW
		;;
	    esac
    fi
}

while getopts f:o: option
do
case "${option}"
in
    f) FILE=${OPTARG};;
    o) OWNER=${OPTARG};;
esac
done

# exit when no valid file was given or owner doesn't match the expected user
if [ -z "$FILE" ] || [ "$OWNER" != "$USER" ]; then
    rm $LOCKFILE
    exit 1
fi

# we only update the overview file if the modified "Next Steps.md" was really in a projects folder
if [[ $FILE == *"Notes/nextcloud/Projects/"*"/Next Steps.md" ]]; then
    # Write headline of Overview file
    echo $'Overview\n========' > $OVERVIEW

    # collect all the next steps and write them to the overview file
    find "$PROJECTS" -maxdepth 1 -mindepth 1 -type d | while read f; do collectNextSteps "$f"; done

    # add a footer to the overview file
    echo $'\n\n' >> $OVERVIEW
    echo "Updated: " $(date)$'\n\n' >> $OVERVIEW

    # Rescan overview file
    php /var/www/nextcloud/occ files:scan --path="$OVERVIEW_RELATIVE" &> /dev/null
fi

rm $LOCKFILE
